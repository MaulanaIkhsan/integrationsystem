<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/jquery.dataTables.min.css'); ?>" />

<script src="<?php echo base_url('assets/adminLTE/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap.min.js') ?>"></script>

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-o"></i> <strong>Pemesanan Darah Masuk</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
                <table id="tabelku">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Instansi</center></th>
                      <th><center>Tanggal Pesan</center></th>
                      <th><center>Jam</center></th>                      
                      <th><center>Petugas</center></th>
                      <th><center>Total</center></th>
                      <th><center>Status</center></th>
                      <th><center>Pengiriman</center></th>
                      <th class="text-center">Operasi</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
                   $no = 1;
                   foreach ($data_pemesanan as $value) : ?>
                     <tr>
                            <td><center><?php echo $no; ?></center></td>
                            <td><center><?php echo $value->consumer_name; ?></center></td>
                            <td><center><?php echo date("d-M-Y", strtotime($value->request_date)); ?></center></td>
                            <td><center><?php echo $value->request_time; ?></center></td>
                            <td><center><?php echo $value->consumer_employee; ?></center></td>
                            <td><center><?php echo $value->request_total; ?></center></td>
                            <td><center><?php echo $value->request_status; ?></center></td>
                            <td class="text-center"> <?php if ($value->request_total < 50) { ?>
                                <small class="label bg-green">Mandiri</small>
                              <?php } else { ?>
                                <small class="label bg-yellow">Antar</small>
                              <?php } ?></td>
                            <?php 
                            if ($value->request_status == 'process') {?>
                                <td colspan="2">
                                <center><a href=<?php echo site_url('pemesanan/kirim_data/'.$value->request_id); ?> class="btn btn-success"><i class="fa fa-envelope"></i> Lanjutkan</a></center>
                                </td>
                            <?php }
                            else { ?>
                              <td>
                                <center>
                                  <a href="<?php echo site_url('pemesanan/detail/'.$value->request_id)?>" class="btn btn-primary"><i class="fa fa-question"></i> Detail</a> | 
                                  <a href=# data-toggle="modal" data-target="<?php echo '#modal_'.$no; ?>" class="btn btn-success"><i class="fa fa-send-o"></i> Proses</a>
                                </center>
                              </td>
                            <?php }?>
                          </tr>

                          <div class="modal fade" id="<?php echo 'modal_'.$no; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Konfirmasi proses</h4>
                                </div>
                                <div class="modal-body">
                                  Apakah anda ingin memproses pemesanan tgl "<?php echo date("d-M-Y", strtotime($value->request_date)); ?>" dari  "<?php echo $value->consumer_name; ?>" ?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Tidak</button>
                                  <a href="<?php echo site_url('pemesanan/execute_request/'.$value->request_id); ?>" class="btn btn-primary"><i class="fa fa-check"></i> Ya, Proses</a>
                                </div>
                              </div>
                            </div>
                          </div>
                   <?php 
                   $no++;
                   endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div> <!-- close box-body -->
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>


</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<script type="text/javascript">
  $(document).ready(function(){
    var data_persediaan = $("#tabelku").DataTable({
                            "pageLength": 25
                          });

    setInterval(function test(){
      data_persediaan.fnDraw();
    }, 100);
  });
</script>

<?php $this->load->view('template/footer'); ?>
