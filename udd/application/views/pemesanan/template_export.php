<!DOCTYPE html>
<html>
<head>
  <title>Formulir</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/self/css/style_export_a4.css'); ?>" />
  <style type="text/css">
    table {
      border-collapse: none;
      border : none;
    }
  </style>
</head>
<body>
  <div id="header">
    <div id="header-logo">
      <img src="<?php echo base_url('assets/self/img/cross.png') ?>" height="80px" />
    </div>
    <div id="alamat">
      <strong>UNIT DONOR DARAH</strong> <br/>
      <strong>PMI KOTA SEMARANG</strong> <br/>
      Jl. Harimau No 10 Semarang Telp 024-1234567 50111     
    </div>
    <hr size="3" width="500" />
  </div>

  <div id="footer">
    <p class="page"> </p>
  </div>

  <div id="content">
    <p>
      <div id="tgl-formulir">
        Semarang , <?php echo date('d-M-Y'); ?>
      </div>
      <div id="paragraph">
        Dengan hormat   
      </div>
      <div>&nbsp;</div>
      <div id="paragraph">
        Kami pihak Bank Darah Rumah Sakit Satu Kota Semarang, telah menerima kantung darah dengan rincian:
      </div>
      <div>&nbsp;</div>
      <div id="paragraph-rinci">
        Pemesanan Kode  &nbsp; &nbsp; &nbsp; &nbsp; :  <?php echo $pemesanan->request_id; ?><br/>
        Konsumen &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:  <?php echo $pemesanan->consumer_name; ?><br/>
        Komponen Darah &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;:  WB<br/>
        Tanggal Pemesanan &nbsp; &nbsp; : <?php echo date("d-M-Y", strtotime($pemesanan->request_date)); ?> <br/>
        Detail &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  : 
      </div>
      <div>&nbsp;</div>
      <div id="paragraph">
        <table class="table-rincian">
          <thead>
            <tr>
              <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
              <th colspan="2"><center>Jenis Kantung</center></th>
              <th rowspan="2"><center>Pengiriman</center></th>
              <th rowspan="2"><center>Total</center></th>
            </tr>
            <tr>
              <th><center>Single</center></th>
              <th><center>Double</center></th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $jumlah_kirim = 0;
            $total_kirim  = 0;
            foreach($detail_pemesanan as $value): ?>
              <tr>
                <td><center><?php echo $value->golongan_darah; ?></center></td>
                <td><center><?php echo $value->total_single; ?></center></td>
                <td><center><?php echo $value->total_double; ?></center></td>
                <td><center><?php echo $value->total_kirim; ?></center></td>
                <td><center><?php echo $value->jumlah_total; ?></center></td>
              </tr>
            <?php 
            $jumlah_kirim = $jumlah_kirim + $value->total_kirim;
            $total_kirim  = $total_kirim + $value->jumlah_total;
            endforeach; ?>
            <tr>
              <td colspan="3"><center><strong>Jumlah Total</strong></center></td>
              <td><center><strong><?php echo $jumlah_kirim; ?></strong></center></td>
              <td><center><strong><?php echo $total_kirim; ?></strong></center></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div>&nbsp;</div>
      <div id="paragraph">
        Atas kerjasamanya diucapkan terima kasih.
      </div>
      <div>&nbsp;</div>
      <div id="ttd-pertama">
        Petugas BDRS
        <div id="nama-petugas">
          <?php echo $pemesanan->consumer_employee; ?>
        </div>
        
      </div>
    </p>
  </div>
  
</body>
</html>