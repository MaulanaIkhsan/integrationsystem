<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-question"></i> <strong>Detail Pemesanan Darah Terkirim</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-5">
              	<div class="pull-right">Tanggal</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo date("d-M-Y", strtotime($detail_pemesanan->request_date)); ?></div>
              </div> 
              <div class="col-md-5">
                <div class="pull-right">Jam</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left"><?php echo $detail_pemesanan->request_time; ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Instansi</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail_pemesanan->consumer_name; ?></div>
              </div>
              <div class="col-md-5">
                <div class="pull-right">Nama Petugas</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left"><?php echo $detail_pemesanan->consumer_employee; ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Status</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail_pemesanan->request_status; ?></div>
              </div>
              <div class="col-md-5">
                <div class="pull-right">Pengiriman</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left">
                  <?php if ($detail_pemesanan->request_total <= 50) { ?>
                  <small class="label bg-green">Mandiri</small>
                  <?php } else { ?>
                  <small class="label bg-yellow">Antar</small>
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-5">
              	<div class="pull-right">Komponen : <?php echo $detail_category->category_name; ?></div>
              </div>
              <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12">
                  <table>
                    <thead>
                      <tr>
                        <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
                        <th colspan="2"><center>Jenis Kantung</center></th>
                        <th rowspan="2"><center>Jumlah Kirim</center></th>
                        <th rowspan="2"><center>Total</center></th>
                      </tr>
                      <tr>
                        <th><center>Single</center></th>
                        <th><center>Double</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $jumlah_kirim = 0;
                      $total_kirim  = 0;
                      foreach($detail_sent as $value): ?>
                        <tr>
                          <td><center><?php echo $value->golongan_darah; ?></center></td>
                          <td><center><?php echo $value->total_single; ?></center></td>
                          <td><center><?php echo $value->total_double; ?></center></td>
                          <td><center><?php echo $value->total_kirim; ?></center></td>
                          <td><center><?php echo $value->jumlah_total; ?></center></td>
                        </tr>
                      <?php 
                      $jumlah_kirim = $jumlah_kirim + $value->total_kirim;
                      $total_kirim  = $total_kirim + $value->jumlah_total;
                      endforeach; ?>
                      <tr>
                        <td colspan="3"><center><strong>Jumlah Total</strong></center></td>
                        <td><center><strong><?php echo $jumlah_kirim; ?></strong></center></td>
                        <td><center><strong><?php echo $total_kirim; ?></strong></center></td>
                      </tr>
                    </tbody>
                  </table>
                </div>

              </div>
              <div class="box-footer">
                <a href="<?php echo site_url('pemesanan/show_sent'); ?>" class="btn btn-danger pull-left"><i class="fa fa-backward"></i> Kembali</a>
                 <a href="<?php echo site_url('pemesanan/export_pdf/'.$detail_pemesanan->request_id); ?>" class="btn btn-warning pull-right" target="blank"><i class="fa fa-file-pdf-o"></i> Formulir</a>
              </div> <!-- close box-footer -->
            </div>
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
