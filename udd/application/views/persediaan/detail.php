<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/jquery.dataTables.min.css'); ?>" />
<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-question"></i> <strong>Detail Persediaan Darah</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-9 pull-left">
                <div class="col-md-3">
                Komponen Darah
                </div>
                <div class="col-md-3">: <?php echo $komponen; ?></div>
                <br/>
                <div class="col-md-3">
                  Golongan Darah
                </div>
                <div class="col-md-3">: <?php echo $golongan_darah; ?></div>
                <br/>
                <div class="col-md-3">
                  Jenis Kantung
                </div>
                <div class="col-md-3">: <?php echo $jenis_kantung; ?></div>
              </div>

              <div class="col-md-3">
                <a href="<?php echo site_url('persediaan'); ?>" class="btn btn-danger pull-right">
                  <i class="fa fa-backward"></i> Kembali
                </a>
              </div>

              <div>&nbsp;</div>
              <div>&nbsp;</div>
              <div class="col-md-12">
                <table id="tabelku">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>No Barcode</center></th>
                      <th><center>Tgl Aftap</center></th>
                      <th><center>Komponen</center></th>
                      <th><center>Kantung</center></th>
                      <th><center>Gol Darah</center></th>
                      <th><center>Rhesus</center></th>
                      <th><center>Tgl Expire</center></th>
                      <th><center>Umur</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $no = 1;
                    foreach ($detail_persediaan as $value):?>
                      <tr>
                        <td><center><?php echo $no; ?></center></td>
                        <td><center><?php echo $value->stock_barcode; ?></center></td>
                        <td><center><?php echo date('d-M-Y', strtotime($value->stock_date_aftap)); ?></center></td>
                        <td><center><?php echo $value->category_symbol; ?></center></td>
                        <td><center><?php echo $value->pocket_name; ?></center></td>
                        <td><center><?php echo $value->type_name; ?></center></td>
                        <td><center><?php echo $value->stock_rhesus; ?></center></td>
                        <td><center><?php echo date('d-M-Y', strtotime($value->stock_date_expired)); ?></center></td>
                        <td><center><?php echo $value->umur_darah.' hari' ?></center></td>
                      </tr>
                    <?php
                    $no++;
                    endforeach;
                    ?>
                  </tbody>
                </table>
              </div> <!-- close col-md-12 -->
            </div> <!-- close box-body -->
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var data_persediaan = $("#tabelku").DataTable({
                            "pageLength": 25
                          });

    setInterval(function test(){
      data_persediaan.fnDraw();
    }, 1000);
  });
</script>

<?php $this->load->view('template/footer'); ?>
