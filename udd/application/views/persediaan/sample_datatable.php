<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/jquery.dataTables.min.css'); ?>" />

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file"></i> <strong>Sample Datatable</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body ">
              <table id="tabelku" >
                <thead>
                  <tr>
                    <th class="text-center">No Barcode</th>
                    <th class="text-center">Tgl Aftap</th>
                    <th class="text-center">Komponen</th>
                    <th class="text-center">Kantung</th>
                    <th class="text-center">Gol Darah</th>
                    <th class="text-center">Rhesus</th>
                    <th class="text-center">Tgl Expired</th>
                    <th class="text-center">Umur</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data_persediaan as $row) : ?>
                    <tr>
                      <td class="text-center"><?php echo $row->stock_barcode; ?></td>
                      <td class="text-center"><?php echo $row->stock_date_aftap; ?></td>
                      <td class="text-center"><?php echo $row->category_symbol; ?></td>
                      <td class="text-center"><?php echo $row->pocket_name; ?></td>
                      <td class="text-center"><?php echo $row->type_name; ?></td>
                      <td class="text-center"><?php echo $row->stock_rhesus; ?></td>
                      <td class="text-center"><?php echo $row->stock_date_expired; ?></td>
                      <td class="text-center"><?php echo $row->umur_darah.' hari'; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              
            </div> <!-- close box-body -->
          </div> <!-- close box-danger -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var data_persediaan = $("#tabelku").DataTable({
                            "pageLength": 25
                          });

    setInterval(function test(){
      data_persediaan.fnDraw();
    }, 1000);
  });
</script>

<?php $this->load->view('template/footer'); ?>
