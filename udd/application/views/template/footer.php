		<!-- jQuery 2.2.0 -->
		
		<!-- jQuery UI 1.11.4 -->
		<script src="<?php echo base_url('assets/adminLTE/dist/js/jquery-ui.min.js'); ?>"></script> 
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
		  $.widget.bridge('uibutton', $.ui.button);
		</script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo base_url('assets/adminLTE/bootstrap/js/bootstrap.min.js'); ?>"></script>


		<!-- AdminLTE App -->
		<script src="<?php echo base_url('assets/adminLTE/dist/js/app.min.js'); ?>"></script>
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script src="<?php echo base_url('assets/adminLTE/dist/js/pages/dashboard.js'); ?>"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="<?php echo base_url('assets/adminLTE/dist/js/demo.js'); ?>"></script>
	</body>
</html>