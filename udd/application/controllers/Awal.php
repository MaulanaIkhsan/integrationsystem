<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Awal extends CI_Controller {

	public function index()
	{
		$this->load->model('Persediaan_model', 'persediaan');
		
		$this->load->model('Pemesanan_model', 'pemesanan');

		$data['jml_persediaan'] = $this->persediaan->get_persediaan();
		$data['category'] 		= $this->persediaan->get_persediaan_komponen();
		$data['jml_masuk']		= $this->pemesanan->count_pemesanan_masuk();
		$data['jml_keluar']		= $this->pemesanan->count_pemesanan_keluar();

		$this->load->view('beranda/before_login', $data);
	}

	public function sample()
	{
		$password = 'petugasA';
		echo md5($password);
	}

}

/* End of file Awal.php */
/* Location: ./application/controllers/Awal.php */