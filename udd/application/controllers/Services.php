<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	//fungsi yang pertama kali di-load ketika object 'Service' dipanggil
	public function __construct()
	{
		parent::__construct();

		$ns = site_url('services');

		//load library NuSOAP
		$this->load->library('nusoap_library');

		$this->nusoap_server = new soap_server;

		//configure our WSDL
		$this->nusoap_server->configureWSDL("Web Service Sistem UDD", $ns);
		
		//set our namespace
		$this->nusoap_server->wsdl->shemaTargetNamespace = $ns;
		
		//Create a complex type
		$this->nusoap_server->wsdl->addComplexType(
				'arrayDetail',
				'complexType',
				'struct',
				'all',
				'',
				array(
					'type_A' 	=> array('name' => 'type_A', 'type'=>'xsd:int'),
					'type_B'	=> array('name' => 'type_B', 'type'=>'xsd:int'),
					'type_O'	=> array('name' => 'type_O', 'type'=>'xsd:int'),
					'type_AB' 	=> array('name' => 'type_AB', 'type'=>'xsd:int')
				)
			);

		$this->nusoap_server->wsdl->addComplexType(
				'arrayEdit',
				'complexType',
				'struct',
				'all',
				'',
				array(
					'request_id' 	=> array('name' => 'request_id', 'type' => 'xsd:string'),
					'category_name'=> array('name' => 'category_blood', 'type' => 'xsd:string'),
					'total_request'	=> array('name' => 'total_request', 'type'=>'xsd:int'),
					'detail_update'	=> array('name' => 'detail_update', 'type'=>'tns:arrayDetail')
				)
			);

		$this->nusoap_server->wsdl->addComplexType(
				'arrayRequest',
				'complexType',
				'struct',
				'all',
				'',
				array(
					'consumer_name' 	=> array('name' => 'consumer_name', 'type' => 'xsd:string'),
					'consumer_employee'	=> array('name' => 'consumer_employee', 'type' => 'xsd:string'),
					'category_blood'	=> array('name' => 'category', 'type' => 'xsd:string'),
					'total_request' 	=> array('name' => 'total_request', 'type'=>'xsd:int'),
					'detail' 			=> array('name' => 'detail', 'type'=>'tns:arrayDetail')
				)
			);

		
		//Register a method that has parameters and return types
		$this->nusoap_server->register(
				'add_request', // method name
				array('detail' => 'tns:arrayRequest'), // parameter list
				array('return' => 'xsd:string'), // return value(s)
				$ns,// namespace
				"urn:".$ns."/add_request", // soapaction:
				"rpc", // style: rpc or document
				"encoded", // use: encoded or literal
				"Membuat permintaan baru" // description: documentation for the method
			);

		$this->nusoap_server->register(
				'delete_request',
				array('request_id' => 'xsd:string'),
				array('return' => 'xsd:string'),
				$ns,
				"urn:".$ns."/delete_request",
				"rpc",
				"encoded",
				"Membatalakan permintaan"
			);

		$this->nusoap_server->register(
				'update_request',
				array('data_update' => 'tns:arrayEdit'),
				array('return' => 'xsd:string'),
				$ns,
				"urn:".$ns."/update_request",
				"rpc",
				"encoded",
				"Mengubah permintaan"
			);

		$this->nusoap_server->register(
				'konfirm_request',
				array('request_id' => 'xsd:string'),
				array('return' => 'xsd:string'),
				$ns,
				"urn:".$ns."/konfirm_request",
				"rpc",
				"encoded",
				"Mengkonfirmasi permintaan"
			);
	}

	//fungsi yang akan pertama kali ditampilkan pada web browser
	public function index()
	{
		function add_request($detail)
		{
			// $this->load->model('Pemesanan_model', 'pemesanan');
			$CI =& get_instance();
			
			$CI->load->model('Pemesanan_model', 'pemesanan');

			$CI->db->trans_begin();

			if (is_array($detail)) {

				if (empty($detail['consumer_name'])) {
					return 'cancel';
				}

				if (empty($detail['consumer_employee'])) {
					return 'cancel';
				}

				if (empty($detail['category_blood'])) {
					return 'cancel';
				}

				if (empty($detail['total_request'])) {
					return 'cancel';
				}
				
				/*if (empty($detail['detail']['type_A'])) {
					return 'cancel';
				}*/

				$id_pemesanan = $CI->pemesanan->generate_id($detail['consumer_name']);

				$save_request 			= $CI->pemesanan->add_pemesanan($detail, $id_pemesanan);
				$add_shipping_request 	= $CI->pemesanan->add_request_shipping($id_pemesanan);

				//jika berhasil
				if (($save_request == 'ok') && ($add_shipping_request == 'ok')) {
					$CI->db->trans_commit();
					return $id_pemesanan;
				}
				else {
					$CI->db->trans_rollback();
					return 'cancel';
				}
			}
			else {
				return 'harap data yang dikirimkan berupa array';
			}
			
		}

		function delete_request($request_id) 
		{
			$CI =& get_instance();
			$CI->load->model('Pemesanan_model', 'pemesanan');

			if (empty($request_id)) {
				return 'cancel';
			}

			$check_pemesanan = $CI->pemesanan->check_status_pemesanan($request_id);

			if ($check_pemesanan == 'ok') {
				$delete = $CI->pemesanan->delete_request($request_id);
				if ($delete === 'ok' ) {
					return 'ok';
				}
				else {
					return 'cancel';
				}
			}
			else {
				return 'gagal_hapus';
			}
		}

		function update_request($data_update)
		{
			$CI =& get_instance();
			$CI->load->model('Pemesanan_model', 'pemesanan');

			if (is_array($data_update)) {
				$check_pemesanan = $CI->pemesanan->check_status_pemesanan($data_update['request_id']);

				if (empty($data_update['request_id'])) {
					return 'gagal_update';
				}

				if (empty($data_update['category_blood'])) {
					return 'gagal_update';
				}

				if (empty($data_update['total_request'])) {
					return 'gagal_update';
				}
				
				/*if (empty($data_update['detail_update']['type_A'])) {
					return 'gagal_update';
				}*/

				if ($check_pemesanan == 'ok') {
					$update = $CI->pemesanan->update_request($data_update);
					if ($update === 'ok') {
						return 'berhasil';
					}
					else {
						return 'update permintaan belum berhasil, harap cek kembali';
					}
					return 'berhasil';
				}
				else {
					return 'gagal_update';
				}
				
			}
			else {
				return 'harap data yang dikirimkan berupa array';
			}
		}

		function konfirm_request($request_id)
		{
			$CI =& get_instance();
			
			$CI->load->model('Pemesanan_model', 'pemesanan');
			
			if (empty($request_id)) {
				return 'cancel';
			}

			$konfirm = $CI->pemesanan->konfirm_request($request_id);
			
			if ($konfirm) {
				return 'ok';
			}
			else {
				return 'cancel';
			}
		}


		$this->nusoap_server->service(file_get_contents("php://input"));

		/*
		refrensi : 
		https://gist.github.com/pawitp/10002197
		*/
	}
}

/* End of file Services.php */
/* Location: ./application/controllers/Services.php */