<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if (($this->session->userdata('username_udd') == NULL) && ($this->session->userdata('nama_udd') == NULL) && ($this->session->userdata('id_udd') == NULL)) {
			$this->session->set_flashdata('warning','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> Harap login terlebih dahulu ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->model('Persediaan_model', 'persediaan');
		$this->load->model('Pemesanan_model', 'pemesanan');

		$data['jml_persediaan'] = $this->persediaan->get_persediaan();
		$data['category'] 		= $this->persediaan->get_persediaan_komponen();
		$data['jml_masuk']		= $this->pemesanan->count_pemesanan_masuk();
		$data['jml_keluar']		= $this->pemesanan->count_pemesanan_keluar();

		$this->load->view('beranda/after_login', $data);		
	}

	public function sample()
	{
		$this->load->model('Pemesanan_model', 'pemesanan');

		$format 	= 'Y-m-d';
		$now 		= date($format);
		$tomorrow 	= date($format, strtotime("+35 day"));
		
		echo 'aftap : '.$now.'<br/>';
		echo 'expired : '.$tomorrow.'<br/>';

		$no_barcode = '1622632';

	}

}

/* End of file Beranda.php */
/* Location: ./application/controllers/Beranda.php */