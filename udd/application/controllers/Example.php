<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends CI_Controller {

	public function index()
	{
		$this->load->model('Permintaan_model', 'permintaan');

		$id = $this->permintaan->generate_id();
		
		echo $id;
	}

}

/* End of file example.php */
/* Location: ./application/controllers/example.php */