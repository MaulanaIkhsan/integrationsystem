<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		if ($this->session->userdata('username') === NULL) {
			$this->session->set_flashdata('warning','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> Harap login terlebih dahulu ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('login');
		}
		
		$this->load->library('nusoap_library');

	}

	// List all your items
	public function show_minta()
	{
		$this->load->model('Permintaan_model', 'permintaan');
		$data['permintaan'] = $this->permintaan->show_permintaan();
		$this->load->view('permintaan/show_minta', $data);
	}

	// Add a new item
	public function add()
	{
		$this->db->trans_begin();

		$this->load->library('form_validation');

		$this->form_validation->set_rules('tujuan', 'Tujuan permintaan', 'required');
		$this->form_validation->set_rules('komponen', 'Komponen darah', 'required');
		$this->form_validation->set_rules('golongan_1', 'Golongan darah A', 'required');
		$this->form_validation->set_rules('golongan_2', 'Golongan darah B', 'required');
		$this->form_validation->set_rules('golongan_3', 'Golongan darah O', 'required');
		$this->form_validation->set_rules('golongan_4', 'Golongan darah AB', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->model('Darah_model', 'darah');
			$this->load->model('Pemasok_model', 'pemasok');

			$data['komponen'] = $this->darah->get_komponen();
			$data['pemasok'] = $this->pemasok->get_pemasok();
			
			$this->load->view('permintaan/add', $data);
		} 
		else {
			$this->load->model('Permintaan_model', 'permintaan');

			$golongan_a  = $this->input->post('golongan_1');
			$golongan_b  = $this->input->post('golongan_2');
			$golongan_o  = $this->input->post('golongan_3');
			$golongan_ab = $this->input->post('golongan_4');
			$total 		 = $golongan_a + $golongan_b + $golongan_o + $golongan_ab;

			$detail_minta = array(
								'type_A' 	=> $golongan_a,
								'type_B' 	=> $golongan_b,
								'type_O' 	=> $golongan_o,
								'type_AB' 	=> $golongan_ab
							);

			$data_minta = array(
							array(
								'consumer_name'  	=> $this->permintaan->get_identitas(),
								'consumer_employee'	=> $this->session->userdata('nama'),
								'category_blood' 	=> 'Whole Blood',
								'total_request'  	=> $total,
								'detail' 		 	=> $detail_minta
							)
						);
			
			$tujuan = $this->permintaan->get_pemasok_url_add($this->input->post('tujuan'));
			
			$client = new nusoap_client($tujuan);

			$error = $client->getError();
		 	if ($error) {
				echo 'error atas: '.$error;
			}

			//proses pemanggilan web service sistem UDD
			$result = $client->call('add_request', $data_minta);
		
			if ($client->fault) {
				echo 'fault : '.$result .'<br/>';
			}
			else {
				$error = $client->getError();
				if ($error) {
					echo 'error bawah : '.$error;
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					
				}
				//jika berhasil tersimpan
				else {
					if ($result == 'cancel') {
						$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Permintaan gagal tersimpan, mohon cek kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
						redirect('permintaan/show_minta/');
					}
					else {
						$simpan_permintaan = $this->permintaan->add_permintaan($result);
						$simpan_pengiriman = $this->permintaan->add_pengiriman($result);

						if (($simpan_permintaan == 'ok5') && ($simpan_pengiriman == 'ok')) {
							$this->db->trans_commit();

							$this->session->set_flashdata('success','<div class="alert alert-info text-center"><i class="fa fa-info-circle"></i> Permintaan berhasil disimpan <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							redirect('permintaan/show_minta/');
						}
						else {
							$this->db->trans_rollback();

							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Permintaan gagal tersimpan, mohon cek kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							redirect('permintaan/show_minta/');
						}
					}
					
				}
			}

			
		}
		
	}

	public function detail($id)
	{
		$this->load->model('Permintaan_model', 'permintaan');

		$data_permintaan		= $this->permintaan->detail($id);
		$detail_data_permintaan	= $this->permintaan->detail_golongan($id);

		if (empty($data_permintaan) or empty($detail_data_permintaan)) {
			$this->session->set_flashdata('error','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> <strong>Detail Permintaan Darah Yang Anda Cari Tidak Tersedia</strong> <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('permintaan/show_minta');
		}
		else {
			$data['detail'] 		 = $this->permintaan->detail($id);
			$data['detail_golongan'] = $this->permintaan->detail_golongan($id);
			$data['detail_komponen'] = $this->permintaan->detail_komponen($id);

			$this->load->view('permintaan/detail', $data);
		}

	}

	//Update one item
	public function edit($id)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tujuan', 'Tujuan permintaan', 'required');
		$this->form_validation->set_rules('komponen', 'Komponen darah', 'required');
		$this->form_validation->set_rules('golongan_0', 'Golongan darah A', 'required');
		$this->form_validation->set_rules('golongan_1', 'Golongan darah B', 'required');
		$this->form_validation->set_rules('golongan_2', 'Golongan darah O', 'required');
		$this->form_validation->set_rules('golongan_3', 'Golongan darah AB', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->model('Darah_model', 'darah');
			$this->load->model('Pemasok_model', 'pemasok');
			$this->load->model('Permintaan_model', 'permintaan');

			$data['komponen'] 		= $this->darah->get_komponen();
			$data['pemasok']  		= $this->pemasok->get_pemasok();
			$data['jml_golongan']	= $this->permintaan->show_edit($id);
			$data['tipe_golongan'] 	= array('A', 'B', 'O', 'AB');
			$data['permintaan_id'] 	= $id;
			
			$this->load->view('permintaan/edit', $data);
		} 
		else {
			$this->load->model('Permintaan_model', 'permintaan');

			$tujuan = $this->permintaan->get_pemasok_url($id);
			
			$id = $this->input->post('id_minta');

			//ambil permintaan kode
			$get_kode 		= $this->permintaan->get_permintaan_kode($id);
			$kode_pemesanan = $get_kode->permintaan_kode;

			$golongan_a 	= $this->input->post('golongan_0');
			$golongan_b 	= $this->input->post('golongan_1');
			$golongan_o 	= $this->input->post('golongan_2');
			$golongan_ab	= $this->input->post('golongan_3');
			$total 			= $golongan_a + $golongan_b + $golongan_o + $golongan_ab;

			$update_detail = array(
								'type_A' 	=> $golongan_a,
								'type_B' 	=> $golongan_b,
								'type_O' 	=> $golongan_o,
								'type_AB'	=> $golongan_ab
							);

			$update_minta = array(
								array(
									'request_id'  	 => $kode_pemesanan,
									'category_name'  => 'Whole Blood',
									'total_request'  => $total,
									'detail_update'	 => $update_detail
								)
							);

			$client = new nusoap_client($tujuan);
			//$client->soap_defencoding = 'UTF-8';

			$error = $client->getError();
		 	if ($error) {
				echo 'error : '.$error;
			}

			$result = $client->call('update_request', $update_minta);
		
			if ($client->fault) {
				echo 'fault : '.$result;
			}
			else {
				$error = $client->getError();
				print_r($update_minta);
				if ($error) {
					echo 'error : '.$error;
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					
				}
				//jika berhasil tersambung
				else {
					if ($this->permintaan->edit_permintaan($id) === 'ok4') {
						$this->session->set_flashdata('success','<div class="alert alert-info text-center"><i class="fa fa-info-circle"></i> Perubahan permintaan berhasil disimpan <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');

						redirect('permintaan/detail/'.$id);
					}
					else{
						$this->session->set_flashdata('error','<div class="alert alert-success text-center"><i class="fa fa-hand-paper-o"></i> Perubahan permintaan gagal tersimpan, mohon cek kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
						
						redirect('permintaan/detail/'.$id);
					}
				}
			}

			// $this->permintaan->edit_permintaan($id);
			

		}
	}

	//Delete one item
	public function delete($id)
	{
		$key = array('permintaan_id' => $id);

		$this->load->model('Permintaan_model', 'permintaan');

		$tujuan = $this->permintaan->get_pemasok_url($id);

		$permintaan_kode = $this->permintaan->get_permintaan_kode($id);
		$request_id 	 = $permintaan_kode->permintaan_kode;

		$client = new nusoap_client($tujuan);
		$error 	= $client->getError();

		$data 	= array('request_id' => $request_id);
		$result = $client->call('delete_request', $data);

		if ($client->fault) {
			// echo 'fault : '.$result;
			$this->session->set_flashdata('success','<div class="alert alert-info text-center"><i class="fa fa-info-circle"></i> Fault : '.$result.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('permintaan/show_minta');

		}
		//jika berhasil tersambung
		else {
			if ($result === 'ok') {
				$hapus_lokal = $this->permintaan->delete_permintaan($key);
				if ($hapus_lokal === 'ok') {
					$this->session->set_flashdata('success','<div class="alert alert-info text-center"><i class="fa fa-info-circle"></i> Permintaan berhasil di batalkan <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
					redirect('permintaan/show_minta');
				}
				else {
					$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-info-circle"></i> Permintaan berhasil di batalkan <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
					redirect('permintaan/show_minta');
				}
			}
			else {
				$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-info-circle"></i> Permintaan berhasil di batalkan <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
				redirect('permintaan/show_minta');
			}
		}
	}

	//menampilkan semua data permintaan dengan status "sent"
	public function show_all_sent()
	{
		$this->load->model('Permintaan_model', 'permintaan');
		
		$data['data_sent']  = $this->permintaan->show_sent_data();
		
		$this->load->view('permintaan/show_sent', $data);
	}

	//menampilkan detail permintaan dengan status "sent"
	public function detail_sent($permintaan_id)
	{
		$this->load->model('Permintaan_model', 'permintaan');

		$permintaan_kode = $this->permintaan->get_permintaan_kode($permintaan_id)->permintaan_kode;

		$data_permintaan		= $this->permintaan->detail($permintaan_id);
		$detail_data_permintaan	= $this->permintaan->get_detail_permintaan($permintaan_kode);
		$detail_data_pengiriman	= $this->permintaan->get_detail_pengiriman($permintaan_kode);

		if (empty($data_permintaan) or empty($detail_data_permintaan) or empty($detail_data_pengiriman)) {
			$this->session->set_flashdata('error','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> <strong>Detail Permintaan Darah Yang Anda Cari Tidak Tersedia</strong> <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('permintaan/show_all_sent');
		}
		else {
			$data['detail'] 		 	= $data_permintaan;
			$data['detail_komponen'] 	= $this->permintaan->detail_komponen($permintaan_id);
			$data['detail_permintaan']	= $detail_data_permintaan;
			$data['detail_pengiriman']	= $detail_data_pengiriman;

			$this->load->view('permintaan/detail_sent', $data);
		}

	}

	//mengkonfirmasi permintaan yang sudah selesai
	public function konfirmasi($permintaan_kode)
	{
		$this->load->model('Permintaan_model', 'permintaan');
		$this->load->model('Persediaan_model', 'persediaan');

		$this->db->trans_begin();

		$client = new nusoap_client('http://127.0.0.1/codeigniter/udd/services');
		$error 	= $client->getError();
		
		if ($error) {
			echo 'error : '.$error;
		}

		$data_konfirm 	= array('request_id' => $permintaan_kode);
		$result 		= $client->call('konfirm_request', $data_konfirm);
		
		if ($client->fault) {
			echo 'fault : '.$result;
		}
		else {
			$update_status_permintaan = $this->permintaan->konfirmasi_permintaan($permintaan_kode);
			$update_status_persediaan = $this->persediaan->update_status_persediaan($permintaan_kode);

			if (($update_status_permintaan == 'ok') && ($update_status_persediaan == 'ok')) {
			
				$this->db->trans_commit();
				
				$this->session->set_flashdata('error','<div class="alert alert-success text-center"><i class="fa fa-info-circle"></i> Permintaan berhasil di konfirmasi <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
				redirect('permintaan/show_all_sent');

			}
			else {
				$this->db->trans_rollback();
				$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-info-circle"></i> Permintaan berhasil di batalkan <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
				redirect('permintaan/show_all_sent');
			}	
		}
		
	}

	//menampilkan daftar riwayat permintaan darah
	public function riwayat()
	{
		$this->load->model('Permintaan_model', 'permintaan');

		$data['data_riwayat'] = $this->permintaan->show_riwayat();

		$this->load->view('permintaan/show_riwayat', $data);
	}

	//menampilkan detail riwayat permintaan darah
	public function detail_riwayat($permintaan_id)
	{
		$this->load->model('Permintaan_model', 'permintaan');

		$permintaan_kode = $this->permintaan->get_permintaan_kode($permintaan_id)->permintaan_kode;

		$data_permintaan		= $this->permintaan->detail_riwayat($permintaan_id);
		$detail_data_permintaan	= $this->permintaan->get_detail_permintaan($permintaan_kode);
		$detail_data_pengiriman	= $this->permintaan->get_detail_pengiriman($permintaan_kode);

		if (empty($data_permintaan) or empty($detail_data_permintaan) or empty($detail_data_pengiriman)) {
			$this->session->set_flashdata('error','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> <strong>Detail Permintaan Darah Yang Anda Cari Tidak Tersedia</strong> <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('permintaan/riwayat');
		}
		else {
			$data['detail'] 		 	= $data_permintaan;
			$data['detail_komponen'] 	= $this->permintaan->detail_komponen($permintaan_id);
			$data['detail_permintaan']	= $detail_data_permintaan;
			$data['detail_pengiriman']	= $detail_data_pengiriman;

			$this->load->view('permintaan/detail_riwayat', $data);
		}
	
	}

	public function export_pdf($permintaan_id)
	{
		$this->load->library('PdfGenerator');

		$this->load->model('Permintaan_model', 'permintaan');

		$permintaan_kode = $this->permintaan->get_permintaan_kode($permintaan_id)->permintaan_kode;

		$data['detail_pengiriman']	= $this->permintaan->get_detail_pengiriman($permintaan_kode);
		$data['permintaan']			= $this->permintaan->get_rincian_export_awal($permintaan_id);
		$data['golongan_a']			= $this->permintaan->get_darah_formulir($permintaan_id, 'A');
		$data['golongan_b']			= $this->permintaan->get_darah_formulir($permintaan_id, 'B');
		$data['golongan_o']			= $this->permintaan->get_darah_formulir($permintaan_id, 'O');
		$data['golongan_ab']		= $this->permintaan->get_darah_formulir($permintaan_id, 'AB');
		
		$html = $this->load->view('permintaan/template_export', $data, true);

		$this->pdfgenerator->generate($html, 'formulir penerimaan');

		// link :
		// http://stackoverflow.com/questions/7484318/header-in-pdf-page-using-dompdf-in-php
	}

	public function sample()
	{
		/*$client = new nusoap_client('http://127.0.0.1/codeigniter/udd/services');
		$error = $client->getError();
		if ($error) {
			echo 'error : '.$error;
		}

		$detail = 
				array('type_A' => '10', 'type_B' => '20', 'type_O'=>'30', 'type_AB'=>'40'
			);

		$data = array(
				array(
					'request_id'  	 => 'rs1201606212102060042',
					'category_blood' => 'Whole Blodd',
					'total_request'  => '100',
					'detail_update'	 => $detail
				)
			);
		
		$result = $client->call('update_request', $data);
		
		if ($client->fault) {
			echo 'fault : '.$result;
		}
		else {
			$error = $client->getError();
			if ($error) {
				// Display the debug messages
				echo '<h2>Request</h2>';
				echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
				echo '<h2>Response</h2>';
				echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
			}
			else {
				echo $result;
			}
		}
*/
		// In an INSERT trigger, only NEW.col_name can be used; there is no old row. In a DELETE trigger, only OLD.col_name can be used; there is no new row. In an UPDATE trigger, you can use OLD.col_name to refer to the columns of a row before it is updated and NEW.col_name to refer to the columns of the row after it is updated. 

		$this->load->model('Permintaan_model', 'permintaan');
		$this->load->model('Persediaan_model', 'persediaan');

		$detail_data_pengiriman = array(
							'permintaan_kode' 	=> 'rs1201610211900290023',
							'komponen_darah'	=> 'WB',
							'golongan_darah'	=> 'A',
							'jenis_kantung'		=> 'Single',
							'total'				=> 0
						);

		echo $this->permintaan->add_new_detail_pengiriman($detail_data_pengiriman);

		/*echo $this->permintaan->update_total_pengiriman_golongan('rs1201609050449200003', 'WB', 'B', '10').'<br/>';
		echo $this->db->last_query();
		echo '<br/>';
		echo $this->permintaan->get_pengiriman_golongan_id('rs1201609050449200003', 'WB', 'B').'<br/>';
		echo $this->db->last_query();
		echo '<br/>';*/
	}

}

/* End of file Permintaan.php */
/* Location: ./application/controllers/Permintaan.php */
