<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persediaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	// List all your items
	public function index()
	{
		$this->load->model('Persediaan_model', 'persediaan');

		$data['jml_persediaan'] = $this->persediaan->get_jml_persediaan();

		$this->load->view('persediaan/show', $data);
	}

	public function detail($golongan, $kantung)
	{
		$this->load->model('Persediaan_model', 'persediaan');

		$data['golongan_darah'] = $golongan;
		$data['jenis_kantung']	= $kantung;
		$data['komponen']		= 'Whole Blood (WB)';
		$data['detail_data'] 	= $this->persediaan->get_detail_persediaan($golongan, $kantung);
		
		$this->load->view('persediaan/detail', $data);
	}
	// Add a new item
	public function add()
	{

	}

	//Update one item
	public function update( $id = NULL )
	{

	}

	//Delete one item
	public function delete( $id = NULL )
	{

	}

	public function sample()
	{
		/*
		Menampilkan hari selanjutnya :
		select curdate() + interval 1 day as hari_esok;
		*/
		$this->load->model('Persediaan_model', 'persediaan');

		$data_darah = array(
							'persediaan_id' 				=> '',
							'persediaan_barcode' 			=> '1645280',
							'persediaan_tgl_aftap'			=> '01-Sep-2016',
							'komponen_id'					=> '1',
							'labu_id'						=> '1',
							'golongan_id'					=> '1',
							'persediaan_rhesus'				=> 'P',
							'persediaan_tgl_kadaluwarsa'	=> '06-Oct-2016',
							'permintaan_kode'				=> 'rs1201609050449200003',
							'persediaan_tgl_input'			=> date('Y-m-d'),
							'persediaan_jam_input'			=> date('H:i:s')
						);

		echo $this->persediaan->insert_persediaan($data_darah);
	}
}

/* End of file Persediaan.php */
/* Location: ./application/controllers/Persediaan.php */
