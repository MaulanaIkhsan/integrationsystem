<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$ns = site_url('services');

		$this->load->library('nusoap_library');

		$this->nusoap_server = new soap_server;
		$this->nusoap_server->configureWSDL('Web Service Sistem BDRS 1', $ns);
		$this->nusoap_server->wsdl->schemaTargetNamespace = $ns;

		$this->nusoap_server->wsdl->addComplexType(
				'arrayDataDarah',
				'complexType',
				'struct',
				'all',
				'',
				array(
					'no_barcode' 		=> array('name' => 'no_barcode', 'type'=>'xsd:int'),
					'tgl_aftap'			=> array('name' => 'tgl_aftap', 'type'=>'xsd:string'),
					'komponen_darah'	=> array('name' => 'komponen_darah', 'type'=>'xsd:string'),
					'jenis_kantung' 	=> array('name' => 'jenis_kantung', 'type'=>'xsd:string'),
					'golongan_darah' 	=> array('name' => 'golongan_darah', 'type'=>'xsd:string'),
					'rhesus' 			=> array('name' => 'rhesus', 'type'=>'xsd:string'),
					'tgl_expire' 		=> array('name' => 'tgl_expire', 'type'=>'xsd:string')
				)
			);

		$this->nusoap_server->wsdl->addComplexType(
				'arrayInputPersediaan',
				'complexType',
				'struct',
				'all',
				'',
				array(
					'pemesanan_kode' => array('name' => 'request_id', 'type' => 'xsd:string'),
					'data_darah'	 => array('name' => 'data_darah', 'type' => 'tns:arrayDataDarah'),
				)
			);

		$this->nusoap_server->wsdl->addComplexType(
				'arrayInputDetailPengiriman',
				'complexType',
				'struct',
				'all',
				'',
				array(
					'permintaan_kode' 	=> array('name' => 'permintaan_kode', 'type' => 'xsd:string'),
					'komponen_darah'	=> array('name' => 'komponen_darah', 'type' => 'xsd:string'),
					'golongan_darah'	=> array('name' => 'golongan_darah', 'type' => 'xsd:string'),
					'jenis_kantung'		=> array('name' => 'jenis_kantung', 'type' => 'xsd:string'),
					'total'				=> array('name' => 'total', 'type' => 'xsd:int')
				)
			);

		$this->nusoap_server->register(
				'ubah_status',
				array('kode_pesan' => 'xsd:string', 'status' => 'xsd:string'),
				array('return' => 'xsd:string'),
				$ns,
				"urn:".$ns.'ubah_status',
				"rpc", 
				"encoded",
				"Mengubah status permintaan"
			);

		$this->nusoap_server->register(
				'ubah_status_pengiriman',
				array('kode_pesan' => 'xsd:string', 'status' => 'xsd:string', 'is_complete' => 'xsd:string'),
				array('return' => 'xsd:string'),
				$ns,
				"urn:".$ns.'ubah_status_pengiriman',
				"rpc",
				"encoded",
				"Mengubah status permintaan dan pengiriman"
			);

		$this->nusoap_server->register(
				'input_persediaan',
				array('data_darah_masuk' => 'tns:arrayInputPersediaan'),
				array('return' => 'xsd:string'),
				$ns,
				"urn:".$ns.'input_persediaan',
				"rpc", 
				"encoded",
				"Memasukkan data darah"
			);

		$this->nusoap_server->register(
				'input_detail_pengiriman',
				array('detail_pengiriman' => 'tns:arrayInputDetailPengiriman'),
				array('return' => 'xsd:string'),
				$ns,
				"urn:".$ns.'input_detail_pengiriman',
				"rpc",
				"encoded",
				"Memasukkan detail data pengiriman"
			);
	}

	public function index()
	{
		function ubah_status($kode_pesan, $status)
		{
			$CI =& get_instance();

			if (empty($kode_pesan)) {
				return 'cancel';
			}

			if (empty($status)) {
				return 'cancel';
			}
			
			$CI->load->model('Permintaan_model', 'permintaan');

			$ubah = $CI->permintaan->ubah_status($kode_pesan, $status);
			if ($ubah === 'ok') {
				return 'ok';
			}
			else {
				return 'cancel';
			}
		}

		function ubah_status_pengiriman($kode_pesan, $status, $is_complete)
		{
			$CI =& get_instance();

			if (empty($kode_pesan)) {
				return 'cancel';
			}

			if (empty($status)) {
				return 'cancel';
			}

			if (empty($is_complete)) {
				return 'cancel';
			}

			$CI->load->model('Permintaan_model', 'permintaan');

			$ubah_permintaan = $CI->permintaan->ubah_status($kode_pesan, $status);
			$ubah_pengiriman = $CI->permintaan->update_pengiriman_complete($kode_pesan, $is_complete);

			if (($ubah_permintaan == 'ok') && ($ubah_pengiriman == 'ok')) {
				return 'ok';
			}
			else {
				return 'cancel';
			}

		}

		//fungsi untuk menyimpan data darah ke tabel "persediaan"
		function input_persediaan($data_darah_masuk)
		{
			$CI =& get_instance();
			
			if (is_array($data_darah_masuk)) {

				if (empty($data_darah_masuk['pemesanan_kode'])) {
					return 'cancel';
				}

				if (empty($data_darah_masuk['data_darah']['no_barcode'])) {
					return 'cancel';
				}

				if (empty($data_darah_masuk['data_darah']['tgl_aftap'])) {
					return 'cancel';
				}

				if (empty($data_darah_masuk['data_darah']['komponen_darah'])) {
					return 'cancel';
				}

				if (empty($data_darah_masuk['data_darah']['jenis_kantung'])) {
					return 'cancel';
				}

				if (empty($data_darah_masuk['data_darah']['golongan_darah'])) {
					return 'cancel';
				}

				if (empty($data_darah_masuk['data_darah']['rhesus'])) {
					return 'cancel';
				}

				if (empty($data_darah_masuk['data_darah']['tgl_expire'])) {
					return 'cancel';
				}

				//load model
				$CI->load->model('Persediaan_model', 'persediaan');

				//mengambil id	
				$komponen_id = $CI->persediaan->get_komponen_id($data_darah_masuk['data_darah']['komponen_darah']);
				$golongan_id = $CI->persediaan->get_golongan_id($data_darah_masuk['data_darah']['golongan_darah']);
				$labu_id 	 = $CI->persediaan->get_labu_id($data_darah_masuk['data_darah']['jenis_kantung']);

				// menyusun berdasarkan struktur tabel persediaan
				$data_darah = array(
									'persediaan_id' 				=> '',
									'persediaan_barcode' 			=> $data_darah_masuk['data_darah']['no_barcode'],
									'persediaan_tgl_aftap'			=> $data_darah_masuk['data_darah']['tgl_aftap'],
									'komponen_id'					=> $komponen_id,
									'labu_id'						=> $labu_id,
									'golongan_id'					=> $golongan_id,
									'persediaan_rhesus'				=> $data_darah_masuk['data_darah']['rhesus'],
									'persediaan_tgl_kadaluwarsa'	=> $data_darah_masuk['data_darah']['tgl_expire'],
									'permintaan_kode'				=> $data_darah_masuk['pemesanan_kode'],
									'persediaan_tgl_input'			=> date('Y-m-d'),
									'persediaan_jam_input'			=> date('H:i:s')
								);

				$add = $CI->persediaan->insert_persediaan($data_darah);
				
				if ($add === 'ok') {
					return 'ok';
				}
				else {
					return 'cancel';
				}

			}
			else {
				return 'cancel';
			}

		}

		function input_detail_pengiriman($detail_pengiriman)
		{
			$CI =& get_instance();
			$CI->load->model('Permintaan_model', 'permintaan');
			
			if (is_array($detail_pengiriman)) {

				if (empty($detail_pengiriman['permintaan_kode'])) {
					return 'cancel';
				}

				if (empty($detail_pengiriman['komponen_darah'])) {
					return 'cancel';
				}

				if (empty($detail_pengiriman['golongan_darah'])) {
					return 'cancel';
				}

				if (empty($detail_pengiriman['jenis_kantung'])) {
					return 'cancel';
				}

				$add_detail = $CI->permintaan->add_new_detail_pengiriman($detail_pengiriman);

				if ($add_detail) {
					return 'ok';
				}
				else {
					return 'cancel';
				}
			}
			else {
				return 'data detail pengiriman harap berupa array';
			}
		}

		$this->nusoap_server->service(file_get_contents("php://input"));
	}

}

/* End of file Services.php */
/* Location: ./application/controllers/Services.php */