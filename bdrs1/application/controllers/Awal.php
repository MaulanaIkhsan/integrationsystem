<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Awal extends CI_Controller {

	public function index()
	{
		$this->load->model('Persediaan_model', 'persediaan');
		$this->load->model('Permintaan_model', 'permintaan');

		$data['permintaan_masuk']	= $this->permintaan->count_permintaan_masuk();
		$data['permintaan_keluar'] 	= $this->permintaan->count_permintaan_keluar();
		$data['jml_persediaan'] 	= $this->persediaan->get_jml_persediaan();
		$this->load->view('beranda/before_login', $data);
	}

}

/* End of file Awal.php */
/* Location: ./application/controllers/Awal.php */