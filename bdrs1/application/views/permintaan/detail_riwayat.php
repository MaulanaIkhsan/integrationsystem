<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Detail Riwayat Permintaan Darah</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-5">
              	<div class="pull-right">Tanggal Pesan</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo date("d-M-Y", strtotime($detail->tgl_pesan)).' '.$detail->permintaan_jam_pesan; ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Petugas</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail->petugas; ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Tujuan</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail->tujuan; ?></div>
              </div>

              <div class="col-md-5">
              	<div class="pull-right">Complete</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail->is_complete; ?></div>
              </div>
              <div class="col-md-5">
                <div class="pull-right">Tanggal Konfirmasi</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left"><?php echo date("d-M-Y", strtotime($detail->pengiriman_tgl_konfirm)).' '.$detail->pengiriman_jam_konfirm; ?></div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-5">
              	<div class="pull-right"><strong>Komponen : <?php echo $detail_komponen->komponen; ?></strong></div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-12">
                <strong>Detail Jumlah Permintaan</strong>
                <p></p>
                <table>
                  <thead>
                    <tr>
                      <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
                      <th colspan="2"><center>Jumlah Permintaan</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $jumlah = 0;
                    foreach ($detail_permintaan as $value) : ?>
                       <tr>
                         <td><center><?php echo $value->golongan_darah; ?></center></td>
                         <td><center><?php echo $value->jumlah_permintaan; ?></center></td>
                       </tr>
                    <?php 
                    $jumlah = $jumlah + $value->jumlah_permintaan;
                    endforeach; ?>
                    <tr>
                      <td><center><strong>Total</strong></center></td>
                      <td><center><?php echo $jumlah; ?></center></td>
                    </tr>
                  </tbody>
                </table>

              </div>

              <div class="col-md-12">
                <strong>Detail Jumlah Pengiriman</strong>
                <p></p>
                <table>
                  <thead>
                    <tr>
                      <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
                      <th colspan="2"><center>Jenis Kantung</center></th>
                      <th rowspan="2"><center>Jumlah Pengiriman</center></th>
                      <th rowspan="2"><center>Total</center></th>
                    </tr>
                    <tr>
                      <th><center>Single</center></th>
                      <th><center>Double</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $jumlah_total_pengiriman = 0;
                    $jumlah_pengiriman       = 0;
                    foreach ($detail_pengiriman as $value) : ?>
                       <tr>
                         <td><center><?php echo $value->golongan_darah; ?></center></td>
                         <td><center><?php echo $value->jumlah_single; ?></center></td>
                         <td><center><?php echo $value->jumlah_double; ?></center></td>
                         <td><center><?php echo $value->total_pengiriman; ?></td>
                         <td><center><?php echo $value->total_semua_pengiriman; ?></center></td>
                       </tr>
                    <?php 
                    $jumlah_pengiriman += $value->total_pengiriman;
                    $jumlah_total_pengiriman += $value->total_semua_pengiriman;
                    endforeach; ?>
                    <tr>
                      <td colspan="3"><center><strong>Total</strong></center></td>
                      <td><center><?php echo $jumlah_pengiriman; ?></center></td>
                      <td><center><?php echo $jumlah_total_pengiriman; ?></center></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer">
              <a href="<?php echo site_url('permintaan/riwayat'); ?>" class="btn btn-danger pull-left"><i class="fa fa-backward"></i> Kembali</a>
              <div class="pull-right">
                <a href="<?php echo site_url('permintaan/export_pdf/'.$detail->id_minta); ?>" class="btn btn-success" target="blank"><i class="fa fa-file-pdf-o"></i> Formulir</a> 
              </div>
            </div>

          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
