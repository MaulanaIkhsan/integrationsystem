<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      
      <!-- Main row -->
      <div class="row">
      
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>

        <div class="col-md-10 col-md-offset-1">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-pencil"></i> <strong>Ubah Permintaan</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <form class="form-horizontal col-md-offset-2" action="<?php echo site_url('permintaan/edit/'.$permintaan_id); ?>" method="post">
              <div class="form-group">
                 <label for="inputTujuan" class="control-label col-md-2">Tujuan :</label>
                 <div class="col-md-5">
                   <select name="tujuan" class="form-control" required="true">
                     <option value="">--Pilih--</option>
                     <?php foreach ($pemasok as $value) { ?>
                       <option value="<?php echo $value->pemasok_id; ?>" selected="true"> <?php echo $value->pemasok_nama; ?> </option>
                     <?php } ?>
                   </select>
                  <span class="text-danger col-md-8"><?php echo form_error('tujuan'); ?></span>
                 </div>
               </div>
               <div class="form-group">
                 <label for="inputKomponen" class="control-label col-md-2">Komponen :</label>
                 <div class="col-md-5">
                   <select name="komponen" class="form-control" required="true">
                     <option value="">--Pilih--</option>
                     <?php foreach ($komponen as $value) { ?>
                       <option value="<?php echo $value->komponen_id; ?>" selected="true"> <?php echo $value->komponen_nama; ?> </option>
                     <?php } ?>
                   </select>
                   <span class="text-danger col-md-8"><?php echo form_error('komponen'); ?></span>
                 </div>
               </div>

               <?php 
                $flag = 0;
                foreach ($jml_golongan as $golongan) { ?>
  
                 <div class="form-group">
                 <label for="inputA" class="control-label col-md-2"><?php echo $tipe_golongan[$flag]; ?> :</label>
                 <div class="col-md-5">
                   <input type="number" name="<?php echo 'golongan_'.$flag; ?>" class="form-control" value="<?php echo $golongan->detail_jumlah; ?>" required="true" />
                   <span class="text-danger col-md-8"><?php echo form_error('golongan_'.$flag); ?></span>
                   <input type="hidden" name="<?php echo 'id_'.$flag; ?>" value="<?php echo $golongan->detail_id; ?>" />
                 </div>
               </div>
               <?php 
                $flag++;
                } ?>

                <!-- untuk mendapatkan nilai dari permintaan_id -->
                <input type="hidden" name="id_minta" value="<?php echo $golongan->permintaan_id; ?>" />

            </div> <!-- close box-body -->
            <div class="box-footer">
              <a href="<?php echo site_url('permintaan/detail/'.$permintaan_id); ?>" class="btn btn-danger pull-left"><i class="fa fa-backward"></i> Kembali</a>
              <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Ubah Permintaan</button>
            </div> <!-- close box-footer -->
          </div> <!-- close box -->
         </form>
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
