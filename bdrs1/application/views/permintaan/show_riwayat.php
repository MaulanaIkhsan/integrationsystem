<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/jquery.dataTables.min.css'); ?>" />

<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap.min.js') ?>"></script>

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-history"></i><strong> Riwayat Permintaan</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
                <table id="tabelku">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Tujuan</center></th>
                      <th><center>Tanggal Pesan</center></th>
                      <th><center>Petugas</center></th>
                      <th><center>Total</center></th>
                      <th><center>Pengiriman</center></th>
                      <th><center>Operasi</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $no = 1;
                    foreach($data_riwayat as $value) :
                    ?>
                      <tr>
                        <td><center><?php echo $no; ?></center></td>
                        <td><center><?php echo $value->tujuan; ?></center></td>
                        <td><center><?php echo date("d-M-Y", strtotime($value->tgl_pesan)); ?></center></td>
                        <td><center><?php echo $value->petugas; ?></center></td>
                        <td><center><?php echo $value->total; ?></center></td>
                        <td><center>
                          <?php if ($value->total < 50) { ?>
                            <small class="label bg-yellow">Mandiri</small>
                          <?php } else { ?>
                            <small class="label bg-green">Antar</small>
                          <?php } ?>
                        </center></td>
                        <td><center><a href="<?php echo site_url('permintaan/detail_riwayat/'.$value->id_minta)?>" class="btn btn-primary"><i class="fa fa-question"></i> Detail</a></center></td>
                      </tr>
                    <?php 
                    $no++;
                    endforeach;
                    ?>
                  </tbody>
                </table>
              </div> <!-- close col-md-12 -->
            </div> <!-- close box-body -->
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->
<script type="text/javascript">
  $(document).ready(function(){
    var data_persediaan = $("#tabelku").DataTable({
                            "pageLength": 25
                          });

    setInterval(function test(){
      data_persediaan.fnDraw();
    }, 1000);
  });
</script>

<?php $this->load->view('template/footer'); ?>
