<!DOCTYPE html>
<html>
<head>
  <title>Formulir</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/self/css/style_export_a4.css'); ?>" />
  <style type="text/css">
    table {
      border-collapse: none;
      border : none;
    }
  </style>
</head>
<body>
  <div id="header">
    <div id="header-logo">
      <img src="<?php echo base_url('assets/self/img/hospital.png') ?>" height="80px" />
    </div>
    <div id="alamat">
      <strong>BANK DARAH</strong> <br/>
      <strong>INSTALASI LABORATORIUM</strong> <br/>
      <strong>RUMAH SAKIT SATU KOTA SEMARANG</strong> <br/>
      Jl. Singa No 10 Semarang Telp 024-1234567 50111     
    </div>
    <hr size="3" width="500" />
  </div>

  <div id="footer">
    <p class="page"> </p>
  </div>

  <div id="content">
    <p>
      <div id="tgl-formulir">
        Semarang , <?php echo date('d-M-Y'); ?>
      </div>
      <div id="paragraph">
        Dengan hormat   
      </div>
      <div>&nbsp;</div>
      <div id="paragraph">
        Kami pihak Bank Darah Rumah Sakit Satu Kota Semarang, telah menerima kantung darah dengan rincian:
      </div>
      <div>&nbsp;</div>
      <div id="paragraph-rinci">
        Permintaan Kode  &nbsp; &nbsp; &nbsp; &nbsp; :  <?php echo $permintaan->permintaan_kode; ?> <br/>
        Pemasok &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : <?php echo $permintaan->pemasok_nama; ?> <br/>
        Komponen Darah &nbsp; &nbsp; &nbsp; &nbsp; : <?php echo $permintaan->komponen_simbol; ?> <br/>
        Tanggal Permintaan &nbsp; &nbsp; : <?php echo date("d-M-Y", strtotime($permintaan->permintaan_tgl_pesan)); ?> <br/>
        Detail &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 
      </div>
      <!--  -->
      
      <div>&nbsp;</div>
      <div id="paragraph">
        <table class="table-rincian">
          <thead>
            <tr>
              <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
              <th colspan="2"><center>Jenis Kantung</center></th>
              <th rowspan="2"><center>Pengiriman</center></th>
              <th rowspan="2"><center>Total</center></th>
            </tr>
            <tr>
              <th><center>Single</center></th>
              <th><center>Double</center></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $jumlah_kirim = 0;
            $total        = 0; 
            foreach ($detail_pengiriman as $value) { ?>
              <tr>
                <td><center><?php echo $value->golongan_darah; ?></center></td>
                <td><center><?php echo $value->jumlah_single; ?></center></td>
                <td><center><?php echo $value->jumlah_double; ?></center></td>
                <td><center><?php echo $value->total_pengiriman; ?></center></td>
                <td><center><?php echo $value->total_semua_pengiriman; ?></center></td>
              </tr>
              <?php 
              $jumlah_kirim += $value->total_pengiriman;
              $total    = $total + $value->total_semua_pengiriman;
            } 
            ?>
            <tr>
              <td colspan="3"><center><strong>Total</strong></center></td>
              <td><center><strong><?php echo $jumlah_kirim; ?></strong></center></td>
              <td><center><strong><?php echo $total; ?></strong></center></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div>&nbsp;</div>
      <div id="paragraph">
        Atas kerjasamanya diucapkan terima kasih.
      </div>
      <div>&nbsp;</div>
      <div id="ttd-pertama">
        Petugas BDRS
        <div id="nama-petugas">
          <?php echo $permintaan->petugas_nama; ?>
        </div>
        
      </div>
      <div id="ttd-kedua">
        Petugas UDD
        <div id="nama-petugas">
          (...........................)
        </div>
      </div>
    </p>

    <p id="page-break">
      <div id="lampiran-kode">
        Permintaan Kode : <?php echo $permintaan->permintaan_kode; ?>
      </div>
      <div id="lampiran-golongan">
        Golongan Darah : A
      </div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div id="paragraph">
        <table class="table-data-darah">
          <thead>
            <tr>
              <th><center>No</center></th>
              <th><center>No Barcode</center></th>
              <th><center>Tgl Aftap</center></th>
              <th><center>Komponen</center></th>
              <th><center>Kantung</center></th>
              <th><center>Gol Darah</center></th>
              <th><center>Rhesus</center></th>
              <th><center>Tgl Expire</center></th>
              <th><center>Cek</center></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($golongan_a as $value) { ?>
              <tr>
                <td><center><?php echo $no; ?></center></td>
                <td><center><?php echo $value->persediaan_barcode; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_aftap)); ?></center></td>
                <td><center><?php echo $value->komponen_simbol; ?></center></td>
                <td><center><?php echo $value->labu_jenis; ?></center></td>
                <td><center><?php echo $value->golongan_nama; ?></center></td>
                <td><center><?php echo $value->persediaan_rhesus; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_kadaluwarsa)); ?></center></td>
                <td>&nbsp;</td>
              </tr>
              <?php 
              $no++;
            } 
            ?>
          </tbody>
        </table>
      </div>
    </p>

    <p id="page-break">
      <div id="lampiran-kode">
        Permintaan Kode : <?php echo $permintaan->permintaan_kode; ?>
      </div>
      <div id="lampiran-golongan">
        Golongan Darah : B
      </div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div id="paragraph">
        <table class="table-data-darah">
          <thead>
            <tr>
              <th><center>No</center></th>
              <th><center>No Barcode</center></th>
              <th><center>Tgl Aftap</center></th>
              <th><center>Komponen</center></th>
              <th><center>Kantung</center></th>
              <th><center>Gol Darah</center></th>
              <th><center>Rhesus</center></th>
              <th><center>Tgl Expire</center></th>
              <th><center>Cek</center></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($golongan_b as $value) { ?>
              <tr>
                <td><center><?php echo $no; ?></center></td>
                <td><center><?php echo $value->persediaan_barcode; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_aftap)); ?></center></td>
                <td><center><?php echo $value->komponen_simbol; ?></center></td>
                <td><center><?php echo $value->labu_jenis; ?></center></td>
                <td><center><?php echo $value->golongan_nama; ?></center></td>
                <td><center><?php echo $value->persediaan_rhesus; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_kadaluwarsa)); ?></center></td>
                <td>&nbsp;</td>
              </tr>
              <?php 
              $no++;
            } 
            ?>
          </tbody>
        </table>
      </div>
    </p>

    <p id="page-break">
      <div id="lampiran-kode">
        Permintaan Kode : <?php echo $permintaan->permintaan_kode; ?>
      </div>
      <div id="lampiran-golongan">
        Golongan Darah : O
      </div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div id="paragraph">
        <table class="table-data-darah">
          <thead>
            <tr>
              <th><center>No</center></th>
              <th><center>No Barcode</center></th>
              <th><center>Tgl Aftap</center></th>
              <th><center>Komponen</center></th>
              <th><center>Kantung</center></th>
              <th><center>Gol Darah</center></th>
              <th><center>Rhesus</center></th>
              <th><center>Tgl Expire</center></th>
              <th><center>Cek</center></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($golongan_o as $value) { ?>
              <tr>
                <td><center><?php echo $no; ?></center></td>
                <td><center><?php echo $value->persediaan_barcode; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_aftap)); ?></center></td>
                <td><center><?php echo $value->komponen_simbol; ?></center></td>
                <td><center><?php echo $value->labu_jenis; ?></center></td>
                <td><center><?php echo $value->golongan_nama; ?></center></td>
                <td><center><?php echo $value->persediaan_rhesus; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_kadaluwarsa)); ?></center></td>
                <td>&nbsp;</td>
              </tr>
              <?php 
              $no++;
            } 
            ?>
          </tbody>
        </table>
      </div>
    </p>

    <p id="page-break">
      <div id="lampiran-kode">
        Permintaan Kode : <?php echo $permintaan->permintaan_kode; ?>
      </div>
      <div id="lampiran-golongan">
        Golongan Darah : AB
      </div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div id="paragraph">
        <table class="table-data-darah">
          <thead>
            <tr>
              <th><center>No</center></th>
              <th><center>No Barcode</center></th>
              <th><center>Tgl Aftap</center></th>
              <th><center>Komponen</center></th>
              <th><center>Kantung</center></th>
              <th><center>Gol Darah</center></th>
              <th><center>Rhesus</center></th>
              <th><center>Tgl Expire</center></th>
              <th><center>Cek</center></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($golongan_ab as $value) { ?>
              <tr>
                <td><center><?php echo $no; ?></center></td>
                <td><center><?php echo $value->persediaan_barcode; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_aftap)); ?></center></td>
                <td><center><?php echo $value->komponen_simbol; ?></center></td>
                <td><center><?php echo $value->labu_jenis; ?></center></td>
                <td><center><?php echo $value->golongan_nama; ?></center></td>
                <td><center><?php echo $value->persediaan_rhesus; ?></center></td>
                <td><center><?php echo date('d-m-Y', strtotime($value->persediaan_tgl_kadaluwarsa)); ?></center></td>
                <td>&nbsp;</td>
              </tr>
              <?php 
              $no++;
            } 
            ?>
          </tbody>
        </table>
      </div>
    </p>
  </div>
  
</body>
</html>