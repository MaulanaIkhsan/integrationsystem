<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav-awal'); ?>
<?php $this->load->view('template/side-nav-awal'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-4 col-md-offset-4 ">
          
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-group"></i><strong> Form Login</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <?php echo $this->session->flashdata('error')?>
              <?php echo $this->session->flashdata('warning'); ?>

              <form action="<?php echo site_url('login'); ?>" method="post">
              
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" placeholder="Username" required="true">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <span class="text-danger"><?php echo form_error('username'); ?></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password" required="true">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="text-danger"><?php echo form_error('password'); ?></span>
              </div>

              <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                  <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-unlock"></i> Masuk</button>
                </div>
                <!-- /.col -->
              </div>
            </form>

            </div>
          </div>

        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
