<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function check_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$result = $this->db->where('petugas_username', $username)
						->where('petugas_password', md5($password))
						->limit(1)
						->get('petugas');

		if ($result->num_rows()>0) {
			return $result->row();
		}
		else {
			return FALSE;
		}
	}	

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */